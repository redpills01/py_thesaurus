"""Unit Testing"""
import unittest
from py_thesaurus import Thesaurus

from py_thesaurus.custom_exceptions import PosTagError

class TestClass(unittest.TestCase):
    """Test Case"""

    def test_valid_entry_check_1(self):
        """
        Check if only numbers entry
        """
        new_inst = Thesaurus('908')
        self.assertFalse(new_inst.valid_entry_check())

    def test_valid_entry_check_2(self):
        """
        Check if only space entry
        """
        new_inst = Thesaurus('      ')
        self.assertFalse(new_inst.valid_entry_check())

    def test_valid_entry_check_3(self):
        """
        Check if only space and numbers entry
        """
        new_inst = Thesaurus('     70  ')
        self.assertFalse(new_inst.valid_entry_check())

    def test_valid_entry_check_4(self):
        """
        Check if only special characters entry
        """
        new_inst = Thesaurus('&*!$^&*%^')
        self.assertFalse(new_inst.valid_entry_check())

    def test_valid_entry_check_5(self):
        """
        Check if only special characters and numbers entry
        """
        new_inst = Thesaurus('&*!$^&*%^7777')
        self.assertFalse(new_inst.valid_entry_check())      

    def test_valid_entry_check_6(self):
        """
        Check if only special characters and numbers and space entry
        """
        new_inst = Thesaurus('&*!$^     &*%^7777')
        self.assertFalse(new_inst.valid_entry_check())           

    def test_create_instance_1(self):
        """
         initialising with Thesaurus object
        """
        self.assertTrue(Thesaurus('Sleeba'))

    def test_get_synonym_1(self):
        """
        Check if an empty list of synonyms is returned for a word (noun) not in vocabulary
        """
        new_inst = Thesaurus("Sleeba")
        self.assertEqual(new_inst.get_synonym(), [])

    def test_get_synonym_2(self):
        """
        Check if an empty list of synonyms is returned for a word (verb) not in vocabulary
        """
        new_inst = Thesaurus("Sleeba")
        self.assertEqual(new_inst.get_synonym(pos="verb"), [])

    def test_get_synonym_3(self):
        """
        Check if an empty list of synonyms is returned for a word (adjective) not in vocabulary
        """

        new_inst = Thesaurus("Sleeba")
        self.assertEqual(new_inst.get_synonym(pos="adj"), [])


    def test_get_synonym_4(self):
        """
        Check if an empty list of synonyms is returned for a word (noun) in vocabulary
        """
        new_inst = Thesaurus("love")
        self.assertTrue(new_inst.get_synonym())

    def test_get_synonym_5(self):
        """
        Check if an empty list of synonyms is returned for a word (verb) in vocabulary
        """
        new_inst = Thesaurus("love")
        self.assertTrue(new_inst.get_synonym(pos="verb"))

    def test_get_synonym_6(self):
        """
        Check if an empty list of synonyms is returned for a word has no adjective in vocabulary

        """
        new_inst = Thesaurus("love")
        self.assertEqual(new_inst.get_synonym(pos="adj"), [])


    def test_get_synonym_7(self):
        """
        Check if an empty list of synonyms is returned for a word (adjective) in vocabulary
        """
        new_inst = Thesaurus("the")
        self.assertTrue(new_inst.get_synonym(pos="adj"))

    def test_get_synonym_8(self):
        """
        Check if a wrong POS tag raises an exception
        """
        new_inst = Thesaurus("dream")
        self.assertRaises(PosTagError,new_inst.get_synonym(pos="adjective"))

    def test_get_synonym_9(self):
        """
        Check if a wrong POS tag riding on boundary raises an exception
        """
        new_inst = Thesaurus("dream")
        self.assertRaises(PosTagError,new_inst.get_synonym(pos="adj1"))


    def test_get_synonym_10(self):
        """
        Check if a None POS tag raises an exception
        """
        new_inst = Thesaurus("dream")
        self.assertRaises(PosTagError,new_inst.get_synonym(pos=None))

    def test_get_definition_1(self):
        """
        Check if definition of the word not in vocabulary can be retrieved from dictionary.com

        """
        new_inst = Thesaurus("Sleeba")
        self.assertEqual(new_inst.get_definition(), [])

    def test_get_definition_2(self):
        """
        Check if definition of the word can be retrieved from
        dictionary.com
        """
        new_inst = Thesaurus("love")
        self.assertTrue(new_inst.get_definition())

    def test_get_antonym_1(self):
        """
        Check if an empty list of antonyms is returned for a word not in vocabulary
        """
        new_inst = Thesaurus("Sleeba")
        self.assertEqual(new_inst.get_antonym(), [])

    def test_get_antonym_2(self):
        """
        Check if a list of antonyms is returned for a word in vocabulary
        """
        new_inst = Thesaurus("love")
        self.assertTrue(new_inst.get_antonym())

    def test_get_antonym_3(self):
        """
        Check if an empty list of antonyms is returned for a word (adjective) in vocabulary
        """
        new_inst = Thesaurus("the")
        self.assertEqual(new_inst.get_antonym(), [])

