#!/bin/bash
#check if all 5 arguments are passed
if [ $# -ne 4 ]
  then
    echo "Missing arguments"
    exit 1
fi
#get build directory path
sourceDirectory="$1"
artifactDirectory="$2"
#get PyPI settings from process variables in CI
pypiusername="$3"
pypipassword="$4"
#cd to the build directory
cd $sourceDirectory
echo "Current directory ===> `pwd`"
#check for python3
# command -v python3 >/dev/null 2>&1 ||  apt install -y python3 || \
# { echo >&2 "I require python3 but it's not installed.  Aborting."; exit 1; }
#check for pip3
# command -v pip3 >/dev/null 2>&1 ||  apt install -y python3-pip || \
# { echo >&2 "I require pip3 but it's not installed.  Aborting."; exit 1; }
# pip3 install twine --user
#build
python3 setup.py sdist --dist-dir $artifactDirectory
#upload
python3 -m twine upload --username $pypiusername --password $pypipassword $artifactDirectory/*