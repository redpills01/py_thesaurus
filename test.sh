#!/bin/bash -e
# bash script with set -e at top will exit if any statement returns a non-zero value
# http://www.davidpashley.com/articles/writing-robust-shell-scripts/
python3 -m unittest -v